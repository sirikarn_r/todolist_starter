FROM java:8
WORKDIR /
ADD /target/scala-2.12/todolist.jar todolist.jar
EXPOSE 9001
