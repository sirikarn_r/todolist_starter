# ** Delete task by Id **

Remove a task by id

## API Flow

* Remove task from database

## URL

|||
| ---------------- | :----------------------------: |
|** Method **      |DELETE                             |
|** Structure **   |`/api/tasks/:id`                |
|** Example **     |`/api/tasks/1`                  |

## Header Params

|Key                  |Value                   |Required      |Description                      |
| ------------------- | :--------------------: | :----------: | ------------------------------- |
|Content-Type         |application/json        |true          |                                 |

## Data Params

|Field Name      |Required      |Type            |Default Value   |Description                                                            |
| -------------- | :----------: | :------------: | -------------- | --------------------------------------------------------------------- |

## Example
```json

```

## Success Response
```json
{
  "data":"Success",
  "code":200
}
```


## Error Response
```json
{
  "error":"Data not found",
  "info":"Data not found",
  "code":404
}
```

