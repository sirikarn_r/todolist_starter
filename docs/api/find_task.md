# ** Find task by Id **

Find a task by id

## API Flow

* Get a task from database

## URL

|||
| ---------------- | :----------------------------: |
|** Method **      |GET                             |
|** Structure **   |`/api/tasks/:id`                |
|** Example **     |`/api/tasks/1`                  |

## Header Params

|Key                  |Value                   |Required      |Description                      |
| ------------------- | :--------------------: | :----------: | ------------------------------- |
|Content-Type         |application/json        |true          |                                 |

## Data Params

|Field Name      |Required      |Type            |Default Value   |Description                                                            |
| -------------- | :----------: | :------------: | -------------- | --------------------------------------------------------------------- |

## Example
```json

```

## Success Response
```json
	{
      "data":{
        "id":"1ea02825-3bd6-4f81-949d-2bcd3d11b750",
        "title":"todolist api",
        "desc":"test desc",
        "status":"pending"
      },
      "code":200
    }
```

## Error Response
```json
{
  "error":"Data not found",
  "info":"Data not found",
  "code":404
}
```

