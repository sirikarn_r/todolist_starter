# ** Empty task data**

Remove all task

## API Flow

* Remove task from database

## URL

|||
| ---------------- | :----------------------------: |
|** Method **      |DELETE                          |
|** Structure **   |`/api/tasks`                    |

## Header Params

|Key                  |Value                   |Required      |Description                      |
| ------------------- | :--------------------: | :----------: | ------------------------------- |
|Content-Type         |application/json        |true          |                                 |

## Data Params

|Field Name      |Required      |Type            |Default Value   |Description                                                            |
| -------------- | :----------: | :------------: | -------------- | --------------------------------------------------------------------- |

## Example
```json

```

## Success Response
```json
{
  "data":"Success",
  "code":200
}
```


