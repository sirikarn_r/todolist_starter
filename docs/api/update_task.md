# ** Update task **

Update task

## API Flow

* Update task data in database

## URL

|||
| ---------------- | :----------------------------: |
|** Method **      |POST                            |
|** Structure **   |`/api/tasks`                    |

## Header Params

|Key                  |Value                   |Required      |Description                      |
| ------------------- | :--------------------: | :----------: | ------------------------------- |
|Content-Type         |application/json        |true          |                                 |

## Data Params

|Field Name      |Required      |Type            |Default Value   |Description                                                            |
| -------------- | :----------: | :------------: | -------------- | --------------------------------------------------------------------- |
|_id            |true           |String          |                |Task id                                      |
|title          |true           |String          |                |Title of task                                |
|desc           |true           |String          |                |Task description                             |
|status         |true           |String          |                |Tash status (pending or dond)                |

## Example
```json
{
                "id": "1ea02825-3bd6-4f81-949d-2bcd3d11b750",
                "title": "todolist api",
                "desc": "test update",
                "status": "pending"
}
```

## Success Response
```json
	{
      "data":{
        "id":"1ea02825-3bd6-4f81-949d-2bcd3d11b750",
        "title":"todolist api",
        "desc":"test update",
        "status":"done"
      },
      "code":200
    }
```

## Error Response
```json
{
  "message":"Fail to parse json.",
  "info":"Process fail.",
  "code":500
}
```

