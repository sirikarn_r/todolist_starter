package controllers

import com.google.inject.Inject
import play.api.mvc._
import service.TaskService
import helper.BodyParser

class TaskController @Inject()(cc: ControllerComponents,service: TaskService)(implicit parser: BodyParser)
  extends AbstractController(cc) {

  def index = Action { implicit request: Request[AnyContent] =>
    service.list match {
      case Right(response) => Ok(parser.jsonParse(response))
      case Left(error) => InternalServerError(parser.parseResponse(error))
    }
  }

  def find(id:String) = Action{
    service.find(id) match {
      case Right(response) => Ok(parser.parseResponse(response))
      case Left(error) => InternalServerError(parser.parseResponse(error))
    }

  }

  def create = Action { implicit request: Request[AnyContent] =>
    parser.jsonToObject(request) match {
      case Left(error) => InternalServerError(parser.parseResponse(error))
      case Right(task) =>{
        service.create(task) match {
          case Right(response) =>  Ok(parser.parseResponse(response))
          case Left(error) => InternalServerError(parser.parseResponse(error))
        }
      }

    }
  }

  def delete(id:String) = Action{ implicit request: Request[AnyContent] =>
    service.delete(id) match {
      case Right(response) => Ok(parser.parseResponse(response))
      case Left(error) => InternalServerError(parser.parseResponse(error))
    }
  }

  def empty = Action{
    service.empty match {
      case Right(response) => Ok(parser.parseResponse(response))
      case Left(error) => InternalServerError(parser.parseResponse(error))
    }
  }

  def update = Action{
    implicit request: Request[AnyContent] =>
      parser.jsonToObject(request) match {
        case Left(error) => InternalServerError(parser.parseResponse(error))
        case Right(task) =>{
          service.update(task) match {
            case Right(response) =>  Ok(parser.parseResponse(response))
            case Left(error) => InternalServerError(parser.parseResponse(error))
          }
        }
      }
  }
}
