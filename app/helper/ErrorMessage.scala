package helper

import play.mvc.Http

object Message{
  val SUCCESS = "Success"
  val DATA_NOT_FOUND = "Data not found."
  val VALIDATE_FAIL = "Validation fail."
  val CREATE_FAIL = "Create data fail."
  val UPDATE_FAIL = "Update data fail."
  val DELETE_FAIL = "Delete data fail."
  val PROCESS_FAIL = "Process fail."
  val DATABASE_FAIL = "Database fail."
  val REQUEST_FAIL = "Fail to parse json."
  val REQUIRE_ID = "ID is required."
  val DATA_DULPICATE = "Duplicate data"
}

trait CustomException{
  val code:Int = Http.Status.SERVICE_UNAVAILABLE
  val message: String = Message.PROCESS_FAIL
  val info: String = Message.PROCESS_FAIL
}

case class DataNotFoundException(override val info:String=Message.DATA_NOT_FOUND) extends CustomException{
  override val code: Int = Http.Status.NOT_FOUND
  override val message: String = Message.DATA_NOT_FOUND
}
case object DatabaseException extends CustomException {
  override val code: Int = Http.Status.INTERNAL_SERVER_ERROR
  override val message: String = Message.DATABASE_FAIL
}
case object DataDuplicateException extends CustomException {
  override val code: Int = Http.Status.INTERNAL_SERVER_ERROR
  override val message: String = Message.DATA_DULPICATE
}
case object CannotCreateDataException extends CustomException{
  override val code: Int = Http.Status.INTERNAL_SERVER_ERROR
  override val message: String = Message.CREATE_FAIL
}
case object CannotUpdateDataException extends CustomException{
  override val code: Int = Http.Status.INTERNAL_SERVER_ERROR
  override val message: String = Message.UPDATE_FAIL
}
case object CannotDeleteDataException extends CustomException{
  override val code: Int = Http.Status.INTERNAL_SERVER_ERROR
  override val message: String = Message.DELETE_FAIL
}
case class BasicValidationException(override val info:String) extends CustomException {
  override val code: Int = Http.Status.INTERNAL_SERVER_ERROR
  override val message: String = Message.VALIDATE_FAIL
}



