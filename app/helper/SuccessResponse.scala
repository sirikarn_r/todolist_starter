package helper

import play.mvc.Http

case class SuccessResponse (data: Any,code:Int = Http.Status.OK)

