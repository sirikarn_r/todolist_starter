package helper

import com.mongodb.DBObject
import model.Task
import net.liftweb.json.{DefaultFormats, Extraction}
import play.api.libs.json.Json
import play.api.mvc.{AnyContent, Request}

import scala.util.{Failure, Success, Try}


class BodyParser {
  implicit val formats = DefaultFormats

  def jsonToObject(request : Request[AnyContent]):Either[ErrorResponse,Task]= {
      val json = request.body.asJson.getOrElse(Left(ErrorResponse(Message.REQUEST_FAIL)))
    json match {
      case Left(ex) => Left(ErrorResponse(Message.REQUEST_FAIL))
      case _ => toTaskObject(json.toString)
      match {
        case Failure(e) => Left(ErrorResponse(Message.REQUEST_FAIL))
        case Success(task) => Right(task)
      }
    }
  }

  def toTaskObject(jsonStr:String):Try[Task]={
    Try{
      net.liftweb.json.parse(jsonStr.toString).extract[Task]
    }
  }

  def parseResponse(response: Any)={
    net.liftweb.json.prettyRender(Extraction.decompose(response))
  }

  def listToJValue(list:List[DBObject]) = {
    list.map(x => net.liftweb.json.parse(x.toString)).map(_.extract[Task])
  }


  def jsonParse(response: Any)={
    Json.parse(net.liftweb.json.prettyRender(Extraction.decompose(response)))
  }
}
