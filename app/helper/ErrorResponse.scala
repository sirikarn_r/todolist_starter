package helper

import play.mvc.Http

case class ErrorResponse(message: String=Message.PROCESS_FAIL,info:Any=Message.PROCESS_FAIL,code:Int=Http.Status.INTERNAL_SERVER_ERROR)
