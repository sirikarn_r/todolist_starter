package model

import play.api.libs.json.Json

case class Task (id:Option[String],title:Option[String],desc:Option[String],status:Option[String]=Some("pending"))
