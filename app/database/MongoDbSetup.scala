package database

import com.mongodb.casbah.{MongoClient, MongoCollection, MongoDB}
import com.typesafe.config.ConfigFactory

object MongoDbSetup {
  private val config = ConfigFactory.load
  private val server = config.getString("mongodb.default.host")
  private val database = config.getString("mongodb.default.db")
  private val client = MongoClient.apply(server)
  private val db:MongoDB = MongoDB.apply(client,database)
  private val collectionName = config.getString("mongodb.default.collection")

  def mongoDB(coll:String = collectionName):MongoCollection = db.apply(coll)
}


