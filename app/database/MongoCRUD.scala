package database

package repository

import java.util.UUID

import com.mongodb.casbah.Imports
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import helper.{CustomException, _}
import model.Task

import scala.util.{Failure, Success, Try}

class MongoCRUD(db:MongoCollection) {
  def findAll:Either[CustomException,List[Imports.DBObject]] ={
    Try(db.find.toList) match {
      case Failure(error) => Left(DatabaseException)
      case Success(data) => Right(data)
    }
  }

  def save(task:Task):Either[CustomException,Task] = {
      Try(db.findOne(MongoDBObject("id" -> task.id))) match {
        case Failure(err) => Left(DatabaseException)
        case Success(data) => data match {
          case Some(_) => Left(DataDuplicateException)
          case None => Try(db.insert(buildMongoDbObject(task))) match {
            case Failure(e) => Left(DatabaseException)
            case Success(t) => Right(task)
          }
        }
      }
  }

  def findById(id:String):Either[CustomException,String] ={
    val q = MongoDBObject("id" -> id)
    Try(db.findOne(q)) match{
      case Failure(error) => Left(DatabaseException)
      case Success(Some(task)) => Right(task.toString)
      case Success(None) => Left(DataNotFoundException())
    }
  }

  def empty:Either[CustomException,String] = {
    Try(db.remove(MongoDBObject.empty)) match {
      case Failure(error) => Left(DatabaseException)
      case Success(_) => Right(Message.SUCCESS)
    }
  }

  def delete(id:String):Either[CustomException,_] ={
    val query = MongoDBObject("id" -> id)
    Try(db.findAndRemove(query)) match{
      case Failure(error) => Left(DatabaseException)
      case Success(None) => Left(DataNotFoundException())
      case Success(task) => Right(Message.SUCCESS)
    }
  }

  def update(task:Task):Either[CustomException,Boolean]={
    val query = MongoDBObject("id" -> task.id)
    Try(db.update(query,buildMongoDbObject(task),upsert = false)) match{
      case Failure(error) => Left(DatabaseException)
      case Success(data) => Right(data.isUpdateOfExisting)
    }
  }

  def buildMongoDbObject(task: Task): MongoDBObject = {
    val builder = MongoDBObject.newBuilder
    builder += "id" -> (
      task.id match {
        case Some(id) => id
        case None => UUID.randomUUID().toString
      })
    builder += "title" -> task.title
    builder += "desc" -> task.desc
    builder += "status" -> task.status
    builder.result
  }
}
