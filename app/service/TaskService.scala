package service

import java.util.UUID

import com.google.inject.Inject
import database.MongoDbSetup
import database.repository.MongoCRUD
import helper.{BodyParser, ErrorResponse, Message, SuccessResponse}
import model.Task
import play.mvc.Http

class TaskService @Inject() (implicit parser: BodyParser){
  val repo: MongoCRUD = new MongoCRUD(MongoDbSetup.mongoDB())

  def list:Either[ErrorResponse,SuccessResponse] ={
    repo.findAll match {
      case Left(error) => Left(ErrorResponse(error.message,error.info,error.code))
      case Right(data) => Right(SuccessResponse(parser.listToJValue(data)))
    }
  }

  def create(task: Task):Either[ErrorResponse,SuccessResponse]={
    val uuid = UUID.randomUUID().toString
    val newTask = Task(Some(uuid),task.title,task.desc,task.status)
    repo.save(newTask) match {
        case Left(error) => Left(ErrorResponse(error.message,error.info,error.code))
        case Right(data) => Right(SuccessResponse(data))
      }
  }

  def find(id:String)(implicit parser:BodyParser):Either[ErrorResponse,SuccessResponse] = {
    repo.findById(id) match {
      case Left(error) => Left(ErrorResponse(error.message,error.info,error.code))
      case Right(data) => Right(SuccessResponse(parser.toTaskObject(data).getOrElse("")))
    }
  }

  def delete(id:String):Either[ErrorResponse,SuccessResponse]={
    repo.delete(id) match {
      case Left(error) => Left(ErrorResponse(error.message,error.info,error.code))
      case Right(data) => Right(SuccessResponse(data))
    }
  }

  def empty:Either[ErrorResponse,SuccessResponse]={
    repo.empty match {
      case Left(error) => Left(ErrorResponse(error.message,error.info,error.code))
      case Right(data) => Right(SuccessResponse(data))
    }
  }

  def update(task:Task):Either[ErrorResponse,SuccessResponse]={
    repo.update(task) match {
      case Left(error) => Left(ErrorResponse(error.message,error.info,error.code))
      case Right(data) => data match {
        case true => Right(SuccessResponse(task))
        case false => Left(ErrorResponse(Message.DATA_NOT_FOUND,Message.DATA_NOT_FOUND,Http.Status.NOT_FOUND))
      }
    }
  }
}
