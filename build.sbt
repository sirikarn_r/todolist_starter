name := "todolist-starter"

organization := "com.example"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  guice,
  "net.liftweb" %% "lift-json" % "3.2.0",
  "net.liftweb" %% "lift-mongodb-record" % "3.2.0",
  "net.liftweb" %% "lift-webkit" % "3.2.0",
  "org.mongodb" %% "casbah" % "3.1.1",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.github.tomakehurst" %  "wiremock" % "1.33" % "test",
  "com.github.fakemongo" %  "fongo" % "2.0.9",
  specs2 % Test,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "com.typesafe.play" %% "play" % "2.5.12",
  "org.mockito" % "mockito-core" % "1.8.5",
  "com.github.tomakehurst" % "wiremock" % "1.33" % "test"
)

javaOptions in Universal += "--Dpidfile.path=/dev/null"

assemblyJarName in assembly := "todolist.jar"
assemblyMergeStrategy in assembly := {
  case x if Assembly.isConfigFile(x) => MergeStrategy.concat
  case PathList(ps @ _*) if Assembly.isReadme(ps.last) || Assembly.isLicenseFile(ps.last) => MergeStrategy.rename
  case PathList("META-INF", xs @ _*) =>
    xs map {_.toLowerCase} match {
      case ("manifest.mf" :: Nil) | ("index.list" :: Nil) | ("dependencies" :: Nil) => MergeStrategy.discard
      case ps @ (x :: _) if ps.last.endsWith(".sf") || ps.last.endsWith(".dsa") => MergeStrategy.discard
      case "plexus" :: _ => MergeStrategy.discard
      case "services" :: _ => MergeStrategy.filterDistinctLines
      case ("spring.schemas" :: Nil) | ("spring.handlers" :: Nil) => MergeStrategy.filterDistinctLines
      case _ => MergeStrategy.first
    }
  case PathList(_*) => MergeStrategy.first
}


mainClass in assembly := Some("play.core.server.ProdServerStart")

fullClasspath in assembly += Attributed.blank(PlayKeys.playPackageAssets.value)
