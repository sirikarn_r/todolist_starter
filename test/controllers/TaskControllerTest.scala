import akka.util.ByteString
import controllers.TaskController
import database.repository.MongoCRUD
import helper.{BodyParser, TaskMock}
import model.Task
import net.liftweb.json.JsonAST.JInt
import net.liftweb.json.{DefaultFormats, JValue}
import org.scalatestplus.play.PlaySpec
import play.api.test.Helpers._
import play.api.mvc._
import play.api.test.{FakeRequest, Helpers}
import repository.RepositoryContext

import scala.concurrent.Future

class TaskControllerTest extends PlaySpec with TaskMock {
  implicit val formats: DefaultFormats.type = DefaultFormats
  implicit val parser:BodyParser = new BodyParser
  implicit val db: RepositoryContext = new RepositoryContext {
    override def taskCollection: MongoCRUD = new MongoCRUD(mockTask)
  }

  val headers: Seq[(String, String)] = List(
    "Content-Type" -> "application/json"
  )
  val uri = "/api/tasks"
  val controller = new TaskController(Helpers.stubControllerComponents())


  trait CreateTask{
    val jsonTask1:String =
      """
        {
          "id": "1",
          "title": "task for update",
          "desc": "",
          "status": "pending"
        }
      """.trim

    val jsonTask2:String =
      """
        {
          "id": "2",
          "title": "task for update",
          "desc": "",
          "status": "pending"
        }
      """.trim
    db.taskCollection.save(parser.toTaskObject(jsonTask1).get)
    db.taskCollection.save(parser.toTaskObject(jsonTask2).get)
  }

  def clearDB: Unit ={
    db.taskCollection.empty
  }

  "Api process" should {
    "success for create task" in {
      val task: Task = Task(Some("task-id"),Some("task title"),Some(""),None)
      val result: Future[Result] = controller.create.apply(generateFakeRequest("POST",uri,Some(task)))
      val response: JValue = net.liftweb.json.parse(bodyContent(result))
      val code = response.\("code")
      clearDB
      assert(code == JInt(200))
    }

    "success for find all task" in new CreateTask {
      val result: Future[Result] = controller.index.apply(generateFakeRequest("GET",uri,None))
      val response: JValue = net.liftweb.json.parse(bodyContent(result))
      val data = response.\("data").values.asInstanceOf[List[Map[String,String]]]
      clearDB
      assert(data.map(t=>t).size == 2)
    }

    "success if task is existed in database" in new CreateTask {
      val result: Future[Result] = controller.find("1").apply(generateFakeRequest("GET",uri,None))
      val response: JValue = net.liftweb.json.parse(bodyContent(result))
      val code = response.\("code")
      clearDB
      assert(code == JInt(200))
    }

    "fail if task is not existed in database" in new CreateTask {
      val result: Future[Result] = controller.find("10").apply(generateFakeRequest("GET",uri,None))
      val response: JValue = net.liftweb.json.parse(bodyContent(result))
      val code = response.\("code")
      clearDB
      assert(code == JInt(404))
    }

    "fail if task is not existed in database for update task" in new CreateTask {
      val t: Task = Task(Some("10"),Some("task title"),Some("update task description"),None)
      val result: Future[Result] = controller.update.apply(generateFakeRequest("PUT",uri,Some(t)))
      val response: JValue = net.liftweb.json.parse(bodyContent(result))
      val code = response.\("code")
      clearDB
      assert(code == JInt(404))
    }

    "success for delete task" in new CreateTask {
      val result: Future[Result] = controller.delete("1").apply(generateFakeRequest("DELETE",uri,None))
      val response: JValue = net.liftweb.json.parse(bodyContent(result))
      val code = response.\("code")
      clearDB
      assert(code == JInt(200))
    }

    "success if empty task complete" in new CreateTask {
      val result: Future[Result] = controller.empty.apply(generateFakeRequest("DELETE",uri,None))
      val response: JValue = net.liftweb.json.parse(bodyContent(result))
      val code = response.\("code")
      clearDB
      assert(code == JInt(200))
    }
  }

  def generateFakeRequest(action:String,uri:String,task:Option[Task]) ={
    task match {
      case Some(data) => FakeRequest(action,uri).withJsonBody(parser.jsonParse(task))
      case None => FakeRequest(action,uri)
    }
  }

  def bodyContent(result: Future[Result]): String={
    val bodyAsBytes: ByteString = contentAsBytes(result)
    bodyAsBytes.decodeString("UTF-8")
  }

}


