package helper

import com.github.fakemongo.Fongo
import com.mongodb.casbah.{MongoCollection, MongoDB}

trait TaskMock {
  val fongo:Fongo = new Fongo("InMemoryTestStorage")
  private val mongo: MongoDB = MongoDB(fongo.getMongo,"fongodb")
  val mockTask: MongoCollection = mongo("in_memory_task")
}
